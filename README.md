# Case editor

Vue implementation of a case editor

## Installing

Using npm:

```bash
$ npm install
```

## Running

To start the mockserver, serving demo data:
```bash
$ npm run mockserver
```
To start the front-end
```bash
$ npm start
```
Watch your console for the port where your application is run

## License

Private, unlicensed